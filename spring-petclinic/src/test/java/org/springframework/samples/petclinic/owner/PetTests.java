
/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.owner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.PetType;
import org.springframework.samples.petclinic.vet.Specialty;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.util.SerializationUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

/**
 * @author Dave Syer
 *
 */
public class PetTests {

	//Declaración de variables necesarias para la prueba de asignar una visita a un veterinario
	public static Vet vetNaranja;
	public static Pet petNaranja;
	public static Owner ownerNaranja;
	public static Visit visitNaranja;
	public static PetType pettypeNaranja;
	public static Specialty specialtyNaranja;
	
	public static Owner ownerBlanco;
	public static Pet petBlanco;
	public static LocalDate birthBlanco = LocalDate.of(2014, 10, 10);
	public static float weightBlanco = (float) 6.0;

	
	public static Pet petMarron;
	public static Owner ownerMarron;
	public static PetType petTypeMarron;
	
	@BeforeClass
	//This method is called before executing this suite
	public static void initClass() {
		
		//Creación del veterinario:
		vetNaranja = new Vet();
		vetNaranja.setId(3013);
		vetNaranja.setFirstName("Juan");
		vetNaranja.setLastName("Sosa");
		vetNaranja.setHomeVisits(true);
		specialtyNaranja = new Specialty();
		specialtyNaranja.setId(3014);
		specialtyNaranja.setName("radiólogo");
		vetNaranja.addSpecialty(specialtyNaranja);
		
		//Creación del propietario:
		ownerNaranja = new Owner();
		ownerNaranja.setId(3009);
		ownerNaranja.setFirstName("Mario");
		ownerNaranja.setLastName("García");
		ownerNaranja.setAddress("Av. de Alemania, 12");
		ownerNaranja.setCity("Cáceres");
		ownerNaranja.setTelephone("555555555");
		
		//Creación de la mascota:
		petNaranja = new Pet();
		petNaranja.setId(3010);
		petNaranja.setName("Piolín");
		LocalDate localdate = LocalDate.now();
		petNaranja.setBirthDate(localdate);
		petNaranja.setComments("Canta raro");
		pettypeNaranja = new PetType(); //tipo de la mascota
		pettypeNaranja.setId(3011);
		pettypeNaranja.setName("canario");
		petNaranja.setType(pettypeNaranja);
		petNaranja.setWeight(20);
		
		//Asigno mascota al propietario:
		ownerNaranja.addPet(petNaranja);
		
		//Creación de la visita:
		visitNaranja = new Visit();
		visitNaranja.setId(3012);
		visitNaranja.setDate(localdate);
		visitNaranja.setDescription("Revisión de las alas");
		visitNaranja.setPetId(petNaranja.getId());
		visitNaranja.setVet(vetNaranja);
		
		/*
		 * Este método se ejecuta antes del test. Inicializa los datos que se utilizarán
		 * para las pruebas. Básicamente, crea un dueño y mascota que serán los
		 * objetivos de la búsqueda
		 */
		
		ownerBlanco = new Owner();
		ownerBlanco.setFirstName("Juan Carlos");
		ownerBlanco.setLastName("Arias");
		ownerBlanco.setAddress("Concordia");
		ownerBlanco.setCity("Caceres");
		ownerBlanco.setTelephone("123456789");

		petBlanco = new Pet();
		petBlanco.setOwner(ownerBlanco);
		petBlanco.setName("Dexter");
		petBlanco.setBirthDate(birthBlanco);
		petBlanco.setWeight(weightBlanco);
		petBlanco.setComments("He likes tennis balls");
		
		
		//Creacion de la mascota (NR1)
		petMarron = new Pet();
		petMarron.setId(2000);
		petMarron.setName("Chuky");
		
		petTypeMarron = new PetType();
		petTypeMarron.setId(2001);
		petTypeMarron.setName("Dog");
		
		petMarron.setType(petTypeMarron);
		petMarron.setWeight(9);
		petMarron.setComments("Prueba");
		LocalDate localDate = LocalDate.now();
		petMarron.setBirthDate(localDate);
		
		//Creacion propietario (NR1)
		ownerMarron = new Owner();
		ownerMarron.addPet(petMarron);
		ownerMarron.setCity("City");
		ownerMarron.setFirstName("Pepe");
		ownerMarron.setLastName("Alfaro");
		ownerMarron.setId(2002);
		ownerMarron.setTelephone("698765432");
		ownerMarron.setAddress("Address");
		
		petMarron.setOwner(ownerMarron);
		

	}
    
    @Test
	public void testVisit() {
		assertNotNull(visitNaranja.getPetId());	
		assertEquals(visitNaranja.getPetId(), petNaranja.getId());		
	}
    
    @Test
   	public void testEditVisit() {
    	LocalDate localdaate = LocalDate.MAX;
    	String des = "Revisión más completa";
    	Vet vetprueba = new Vet();
    	vetprueba.setId(3015);
		vetprueba.setFirstName("Miguel");
		vetprueba.setLastName("González");
		vetprueba.setHomeVisits(true);
		Specialty specialtyprueba = new Specialty();
		specialtyprueba.setId(3016);
		specialtyprueba.setName("radiólogo");
		vetprueba.addSpecialty(specialtyNaranja);
   		visitNaranja.setDate(localdaate);
   		visitNaranja.setDescription(des);
   		visitNaranja.setVet(vetprueba);
   		
   		assertNotNull(visitNaranja.getDate());
   		assertEquals(visitNaranja.getDate(), localdaate);
   		
   		assertNotNull(visitNaranja.getDescription());
   		assertEquals(visitNaranja.getDescription(), des);
   		
   		assertNotNull(visitNaranja.getVet());
   		assertEquals(visitNaranja.getVet(), vetprueba);
   	}
    
    /*
	 * Este método comprueba que se ha creado e insertado correctamente el dueño
	 * creado. Comprueba para todos sus campos si son nulos y si coinciden con los
	 * datos asignados
	 */
	@Test
	public void testOwnerBlanco() {
		assertNotNull(ownerBlanco);

		assertNotNull(ownerBlanco.getFirstName());
		assertEquals(ownerBlanco.getFirstName(), "Juan Carlos");

		assertNotNull(ownerBlanco.getLastName());
		assertEquals(ownerBlanco.getLastName(), "Arias");

		assertNotNull(ownerBlanco.getAddress());
		assertEquals(ownerBlanco.getAddress(), "Concordia");

		assertNotNull(ownerBlanco.getCity());
		assertEquals(ownerBlanco.getCity(), "Caceres");

		assertNotNull(ownerBlanco.getTelephone());
		assertEquals(ownerBlanco.getTelephone(), "123456789");
	}

	/*
	 * Este método comprueba que se ha creado e insertado correctamente la mascota
	 * creado. Comprueba para todos sus campos si son nulos y si coinciden con los
	 * datos asignados
	 */
	@Test
	public void testPetBlanco() {
		assertNotNull(petBlanco);

		assertNotNull(petBlanco.getOwner());
		assertEquals(petBlanco.getOwner(), ownerBlanco);

		assertNotNull(petBlanco.getName());
		assertEquals(petBlanco.getName(), "Dexter");

		assertNotNull(petBlanco.getBirthDate());
		assertEquals(petBlanco.getBirthDate(), birthBlanco);

		assertNotNull(petBlanco.getWeight());
		assertEquals(petBlanco.getWeight(), weightBlanco, 0);

		assertNotNull(petBlanco.getComments());
		assertEquals(petBlanco.getComments(), "He likes tennis balls");
	}	

	@Test
	public void testWeightNR1() {
		assertNotNull(petMarron.getWeight());
		assertTrue(petMarron.getWeight()==9);
	}

	@Test
	public void testCommentsNR1() {
		assertNotNull(petMarron.getComments());
		assertEquals("Prueba",petMarron.getComments());
	}
	
    @AfterClass
    //This method is executed after all the tests included in this suite are completed.
    public static void finishClass() {
    	vetNaranja = null;
    	ownerNaranja = null;
    	petNaranja = null;
    	visitNaranja = null;
    	petBlanco = null;
    	ownerBlanco =null;
    }

}