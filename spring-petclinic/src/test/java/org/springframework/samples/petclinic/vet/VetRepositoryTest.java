package org.springframework.samples.petclinic.vet;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.owner.OwnerRepository;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.PetRepository;
import org.springframework.samples.petclinic.owner.PetType;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.samples.petclinic.visit.VisitRepository;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class VetRepositoryTest {
	
	@Autowired
	private VetRepository vets;
	
	@Autowired
	private OwnerRepository owners;
	
	@Autowired
	private PetRepository pets;
	
	@Autowired
	private VisitRepository visits;
	
	//Declaración de variables necesarias para la prueba de asignar una visita a un veterinario
		private Vet vetNaranja;
		private Pet petNaranja;
		private Owner ownerNaranja;
		private Visit visitNaranja;
		
		private Vet vetNaranja2;
		private Vet vetMarron;
		
	@Before
	//This method is called before executing this suite
	public void init() {
		
		if (this.vetNaranja == null && this.petNaranja == null && this.ownerNaranja == null && this.visitNaranja == null) {
			
			//Creación del veterinario:
			vetNaranja = new Vet();
			vetNaranja.setFirstName("Francisco");
			vetNaranja.setLastName("López");
			vetNaranja.setHomeVisits(true);
			
			Collection <Specialty> vetSpecNaranja = this.vets.findSpecialties();
			
			if (!vetSpecNaranja.isEmpty()) {				
				for (Specialty spec : vetSpecNaranja) {
					vetNaranja.addSpecialty(spec);
				}				
			}
			
			//Creación del propietario:
			ownerNaranja = new Owner();
			ownerNaranja.setFirstName("Pedro");
			ownerNaranja.setLastName("Sánchez");
			ownerNaranja.setAddress("Av. Portugal, 7");
			ownerNaranja.setCity("Cáceres");
			ownerNaranja.setTelephone("333333333");
			
			//Creación de la mascota:
			petNaranja = new Pet();
			petNaranja.setName("Pepi");
			LocalDate localdate = LocalDate.now();
			petNaranja.setBirthDate(localdate);

			Collection <PetType> petTypeNaranja = this.pets.findPetTypes();
			
			if (!petTypeNaranja.isEmpty()) {				
				for (PetType type : petTypeNaranja) {
					petNaranja.setType(type);
				}				
			}
			petNaranja.setWeight(15);
			petNaranja.setComments("Es un poco agresiva");
			
			//Asigno mascota al propietario:
			ownerNaranja.addPet(petNaranja);
					
			//Almacenamos en la base de datos:
			this.vets.save (vetNaranja);
			this.owners.save(ownerNaranja);
			this.pets.save(petNaranja);
			
			//Creación de la visita:
			visitNaranja = new Visit();
			LocalDate localdaate = LocalDate.now();
			visitNaranja.setDate(localdaate);
			visitNaranja.setDescription("Revisión de las orejas");
			visitNaranja.setPetId(petNaranja.getId());
			visitNaranja.setVet(vetNaranja);
			
			this.visits.save(visitNaranja);

			
			
		}
		
		if (this.vetNaranja2 == null) {

			// Creación del veterinario:
			vetNaranja2 = new Vet();
			vetNaranja2.setFirstName("Jesús");
			vetNaranja2.setLastName("Díaz");
			vetNaranja2.setHomeVisits(true);

			Collection<Specialty> vetSpecNaranja = this.vets.findSpecialties();

			if (!vetSpecNaranja.isEmpty()) {
				for (Specialty spec : vetSpecNaranja) {
					vetNaranja2.addSpecialty(spec);
				}
			}

			// Almacenamos en la base de datos:
			this.vets.save(vetNaranja2);

		}

		if (this.vetMarron == null) {
			vetMarron = new Vet();
			vetMarron.setFirstName("Manuel");
			vetMarron.setLastName("Diaz");
			vetMarron.setHomeVisits(false);

			Collection<Specialty> vetSpec = this.vets.findSpecialties();
			if (!vetSpec.isEmpty()) {
				for (Specialty spec : vetSpec) {
					vetMarron.addSpecialty(spec);
				}
			}

			this.vets.save(vetMarron);
		}
		
	}
	
	
	@Test
	public void testFindById() {
		Vet vetNaranjaFindById = this.vets.findById(vetNaranja.getId());
		
		assertNotNull(vetNaranjaFindById.getFirstName());
		assertEquals(vetNaranjaFindById.getFirstName(), vetNaranja.getFirstName());	
		
		assertNotNull(vetNaranjaFindById.getLastName());
		assertEquals(vetNaranjaFindById.getLastName(), vetNaranja.getLastName());	
		
		assertEquals(vetNaranjaFindById.getHomeVisits(), vetNaranja.getHomeVisits());
		
		assertEquals(vetNaranjaFindById.getSpecialties().size(), vetNaranja.getSpecialties().size());
		
		if (vetNaranjaFindById.getSpecialties().size()>0) {
			assertArrayEquals(vetNaranjaFindById.getSpecialties().toArray(), vetNaranja.getSpecialties().toArray());
		}
		
		
		Visit visitNaranjaFindById = this.visits.findById(visitNaranja.getId());
		
		assertEquals(visitNaranjaFindById.getDate(), visitNaranja.getDate());	
		
		assertEquals(visitNaranjaFindById.getDescription(), visitNaranja.getDescription());	
		
		assertEquals(visitNaranjaFindById.getPetId(), visitNaranja.getPetId());
		
		assertNotNull (visitNaranjaFindById.getVet());
		assertEquals(visitNaranjaFindById.getVet(), visitNaranja.getVet());
		
	}
	
	@Test
	public void testFindById2() {
		Vet vetNaranjaFindById = this.vets.findById(vetNaranja2.getId());

		assertNotNull(vetNaranjaFindById.getFirstName());
		assertEquals(vetNaranjaFindById.getFirstName(), vetNaranja2.getFirstName());

		assertNotNull(vetNaranjaFindById.getLastName());
		assertEquals(vetNaranjaFindById.getLastName(), vetNaranja2.getLastName());

		assertNotNull(vetNaranjaFindById.getHomeVisits());
		assertEquals(vetNaranjaFindById.getHomeVisits(), vetNaranja2.getHomeVisits());

		assertEquals(vetNaranjaFindById.getSpecialties().size(), vetNaranja2.getSpecialties().size());

		if (vetNaranjaFindById.getSpecialties().size() > 0) {
			assertArrayEquals(vetNaranjaFindById.getSpecialties().toArray(), vetNaranja2.getSpecialties().toArray());
		}

	}
	
	@Test
	public void testInsertarFindById() {
		
		Vet vetNaranjaFindById = this.vets.findById(vetNaranja.getId());
		
		assertNotNull(vetNaranjaFindById.getFirstName());
		assertEquals(vetNaranjaFindById.getFirstName(), vetNaranja.getFirstName());	
		
		assertNotNull(vetNaranjaFindById.getLastName());
		assertEquals(vetNaranjaFindById.getLastName(), vetNaranja.getLastName());	
		
		assertEquals(vetNaranjaFindById.getSpecialties().size(), vetNaranja.getSpecialties().size());
		
		if (vetNaranjaFindById.getSpecialties().size()>0) {
			assertArrayEquals(vetNaranjaFindById.getSpecialties().toArray(), vetNaranja.getSpecialties().toArray());
		}
		
	}
	
	@Test
	public void testSpecialtyNR2() {
		Vet vetFindById = this.vets.findById(vetMarron.getId());

		assertNotNull(vetFindById.getSpecialties());
		assertNotNull(vetFindById.getSpecialties().get(0));
		assertTrue(this.vets.findSpecialties().contains(vetFindById.getSpecialties().get(0)));

	}

	@Test
	public void testVetNR2() {
		Vet vetFindById = this.vets.findById(vetMarron.getId());

		assertNotNull(vetFindById.getFirstName());
		assertEquals(vetFindById.getFirstName(), vetMarron.getFirstName());

		assertNotNull(vetFindById.getLastName());
		assertEquals(vetFindById.getLastName(), vetMarron.getLastName());

	}
	
	@After
	public void finish() {
		this.vetNaranja=null;
		this.petNaranja=null;
		this.ownerNaranja=null;
		this.visitNaranja=null;

		this.vetMarron = null;
		this.vetNaranja2 = null;
	}
	




	
}
