package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.vet.Specialty;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.samples.petclinic.visit.VisitRepository;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class PetRepositoryTest {

	@Autowired
	private PetRepository pets;

	@Autowired
	private OwnerRepository owners;

	@Autowired
	private VetRepository vets;

	@Autowired
	private VisitRepository visits;

	private Owner ownerBlanco;
	private Pet petBlanco;

	private Pet petAzul;
	private Owner ownerAzul;

	private PetType petTypeBlanco;

	public LocalDate birthBlanco = LocalDate.of(2014, 10, 10);
	public float weightBlanco = (float) 6.0;

	// Declaración de variables necesarias para la prueba de asignar una visita a un
	// veterinario
	private Vet vetNaranja;
	private Pet petNaranja;
	private Owner ownerNaranja;
	private Visit visitNaranja;

	private Pet petMarron;

	private Owner ownerMarron;

	private PetType petTypeMarron;

	/*
	 * Este método se ejecuta antes del test. Inicializa los datos que se utilizarán
	 * para las pruebas. Básicamente, crea un dueño y mascota que serán los
	 * objetivos de la búsqueda
	 */
	@Before
	public void init() {

		/*
		 * Este bloque inicializa las entidades creadas para las pruebas realizadas por
		 * el SrBlanco para el caso de uso NR10 - Buscar mascotas por nombre
		 */
		if (this.petBlanco == null && this.ownerBlanco == null && this.petTypeBlanco == null) {
			this.ownerBlanco = new Owner();
			this.ownerBlanco.setFirstName("Juan Carlos");
			this.ownerBlanco.setLastName("Arias");
			this.ownerBlanco.setAddress("Concordia");
			this.ownerBlanco.setCity("Caceres");
			this.ownerBlanco.setTelephone("123456789");

			this.petBlanco = new Pet();
			this.petBlanco.setName("Dexter");

			this.petTypeBlanco = new PetType();
			List<PetType> listaBlanco = this.pets.findPetTypes();
			this.petTypeBlanco = listaBlanco.get(0);

			this.petBlanco.setType(petTypeBlanco);
			this.petBlanco.setWeight(weightBlanco);
			this.petBlanco.setComments("He likes tennis balls");
			this.petBlanco.setBirthDate(birthBlanco);

			/* Después de crear ambos objetos, los inserta en la base de datos */
			this.ownerBlanco.addPet(petBlanco);
			this.owners.save(ownerBlanco);
			this.pets.save(petBlanco);
		}

		PetType petType = new PetType();
		LocalDate birthDate = LocalDate.now();

		if (this.ownerAzul == null && this.petAzul == null) {

			ownerAzul = new Owner();
			petAzul = new Pet();

			ownerAzul.setFirstName("Antonio");
			ownerAzul.setLastName("MartÃ­nez");
			ownerAzul.setTelephone("927435678");
			ownerAzul.setCity("Madrid");
			ownerAzul.setAddress("Calle Alemania");

			owners.save(ownerAzul);

			petType.setId(1);
			petType.setName("cat");

			Set<Visit> visits = new HashSet<Visit>();
			petAzul.setName("Dafne");
			petAzul.setType(petType);
			petAzul.setWeight(4);
			petAzul.setComments("Puede araÃ±ar");
			petAzul.setBirthDate(birthDate);
			petAzul.setOwner(ownerAzul);
			petAzul.setVisitsInternal(visits);

			ownerAzul.addPet(petAzul);
			Set<Pet> set = new HashSet<Pet>();
			set.add(petAzul);
			ownerAzul.setPetsInternal(set);

			pets.save(petAzul);
		}

		if (this.vetNaranja == null && this.petNaranja == null && this.ownerNaranja == null
				&& this.visitNaranja == null) {

			// Creación del veterinario:
			vetNaranja = new Vet();
			vetNaranja.setFirstName("Juan");
			vetNaranja.setLastName("Sosa");
			vetNaranja.setHomeVisits(true);

			Collection<Specialty> vetSpecNaranja = this.vets.findSpecialties();

			if (!vetSpecNaranja.isEmpty()) {
				for (Specialty spec : vetSpecNaranja) {
					vetNaranja.addSpecialty(spec);
				}
			}

			// Creación del propietario:
			ownerNaranja = new Owner();
			ownerNaranja.setFirstName("Mario");
			ownerNaranja.setLastName("García");
			ownerNaranja.setAddress("Av. de Alemania, 12");
			ownerNaranja.setCity("Cáceres");
			ownerNaranja.setTelephone("555555555");

			// Creación de la mascota:
			petNaranja = new Pet();
			petNaranja.setName("Piolín");
			LocalDate localdate = LocalDate.now();
			petNaranja.setBirthDate(localdate);

			Collection<PetType> petTypeNaranja = this.pets.findPetTypes();

			if (!petTypeNaranja.isEmpty()) {
				for (PetType type : petTypeNaranja) {
					petNaranja.setType(type);
				}
			}
			petNaranja.setWeight(20);
			petNaranja.setComments("Canta raro");

			// Asigno mascota al propietario:
			ownerNaranja.addPet(petNaranja);

			// Almacenamos en la base de datos:
			this.vets.save(vetNaranja);
			this.owners.save(ownerNaranja);
			this.pets.save(petNaranja);

			// Creación de la visita:
			visitNaranja = new Visit();
			LocalDate localdaate = LocalDate.now();
			visitNaranja.setDate(localdaate);
			visitNaranja.setDescription("Revisión de las alas");
			visitNaranja.setPetId(petNaranja.getId());
			visitNaranja.setVet(vetNaranja);

			this.visits.save(visitNaranja);
		}

		if (this.petMarron == null && this.ownerMarron == null && this.petTypeMarron == null) {

			// Creacion propietario (NR1)

			this.ownerMarron = new Owner();
			this.ownerMarron.setFirstName("Pepe");
			this.ownerMarron.setLastName("Alfaro");
			this.ownerMarron.setCity("City");
			this.ownerMarron.setAddress("Address");
			this.ownerMarron.setTelephone("698765432");

			this.petMarron = new Pet();
			this.petMarron.setName("Chuky");

			this.petTypeMarron = new PetType();
			List<PetType> listaTiposMarron = this.pets.findPetTypes();
			this.petTypeMarron = listaTiposMarron.get(0);

			this.petMarron.setType(petTypeMarron);
			this.petMarron.setWeight(9);
			this.petMarron.setComments("Prueba");
			LocalDate localDate = LocalDate.now();
			this.petMarron.setBirthDate(localDate);

			this.ownerMarron.addPet(petMarron);
			this.owners.save(ownerMarron);

			this.pets.save(petMarron);

		}

	}

	/*
	 * Este método comprueba que se ha creado e insertado correctamente el dueño
	 * creado. Comprueba para todos sus campos si no son nulos y si coinciden con
	 * los datos recuperados de la inserción en la base de datos
	 */
	@Test
	public void testOwnerBlanco() {
		Owner aux = owners.findById(ownerBlanco.getId());
		assertNotNull(aux);

		assertNotNull(aux.getFirstName());
		assertEquals(aux.getFirstName(), ownerBlanco.getFirstName());

		assertNotNull(aux.getLastName());
		assertEquals(aux.getLastName(), ownerBlanco.getLastName());

		assertNotNull(aux.getAddress());
		assertEquals(aux.getAddress(), ownerBlanco.getAddress());

		assertNotNull(aux.getCity());
		assertEquals(aux.getCity(), ownerBlanco.getCity());

		assertNotNull(aux.getTelephone());
		assertEquals(aux.getTelephone(), ownerBlanco.getTelephone());
	}

	/*
	 * Este método comprueba que el método findPetsByName funciona correctamente.
	 * Para ello, realiza la búsqueda mediante el nombre que habíamos declarado
	 * anteriormente y almacena el resultado en una colección. En caso de que la
	 * búsqueda funcione el tamaño de la colección será > 0, por lo que se realiza
	 * un bucle iterando por los datos del elemento que existe en la colección y se
	 * realizan sus asertos
	 */

	@Test
	public void testFindPetByName() {
		Collection<Pet> collection = pets.findPetsByName(petBlanco.getName());
		if (collection.size() > 0) {
			System.out.println("Pet found");
			for (Pet aux : collection) {
				assertNotNull(aux);

				assertNotNull(aux.getOwner());
				assertEquals(aux.getOwner(), petBlanco.getOwner());

				assertNotNull(aux.getName());
				assertEquals(aux.getName(), petBlanco.getName());

				assertNotNull(aux.getBirthDate());
				assertEquals(aux.getBirthDate(), petBlanco.getBirthDate());

				assertNotNull(aux.getWeight());
				assertEquals(aux.getWeight(), petBlanco.getWeight(), 0);

				assertNotNull(aux.getComments());
				assertEquals(aux.getComments(), petBlanco.getComments());
			}
		} else
			System.out.println("Pet not found");
	}

	@Test
	public void savePet() {

		Owner ownerTest = new Owner();

		ownerTest.setFirstName("LucÃ­a");
		ownerTest.setLastName("Lucas");
		ownerTest.setTelephone("666666666");
		ownerTest.setCity("Alicante");
		ownerTest.setAddress("Avenida PÃ©rez MartÃ­nez, 12");

		Pet petTest = new Pet();
		PetType petType = new PetType();

		petType.setId(1);
		petType.setName("cat");

		petTest.setName("Bunny");
		petTest.setComments("ComentarioTest");
		petTest.setWeight(3);
		petTest.setBirthDate(LocalDate.now());
		petTest.setOwner(ownerTest);
		petTest.setType(petType);

		ownerAzul.addPet(petTest);

		assertNull(petTest.getId());
		pets.save(petTest);
		assertNotNull(petTest.getId());
	}

	@Test
	public void findByIdPet() {

		Pet petTest = new Pet();
		assertNull(petTest.getId());
		assertNull(petTest.getName());

		petTest = pets.findById(petAzul.getId());
		assertNotNull(petTest.getId());
		assertTrue(petTest.getName().equals("Dafne"));
	}

	@Test
	public void deletePet() {

		Owner ownerTest = owners.findById(ownerAzul.getId());
		assertNotNull(ownerTest);

		List<Pet> petsOwner = ownerTest.getPets();
		assertTrue(petsOwner.size() == 1);

		for (int i = 0; i < petsOwner.size(); i++) {

			pets.deletePetInfo(petsOwner.get(i).getId());
			pets.delete(petsOwner.get(i));
		}
		assertNull(pets.findById(petAzul.getId()));
	}

	@Test
	public void testPetNR1() {

		// Pruebas de la mascota

		Pet petFindById = this.pets.findById(petMarron.getId());

		assertNotNull(petFindById.getName());

		assertEquals(petFindById.getName(), petMarron.getName());

		assertEquals(petFindById.getBirthDate(), petMarron.getBirthDate());

		assertEquals(petFindById.getComments(), petMarron.getComments());

		assertEquals(petFindById.getOwner(), petMarron.getOwner());

		assertEquals(petFindById.getType(), petMarron.getType());

		// Comprobamos peso y comentarios
		assertTrue(petFindById.getWeight() == petMarron.getWeight());
		assertEquals(petFindById.getComments(), petMarron.getComments());

	}

	@Test
	public void testFindById() {

		Pet petNaranjaFindById = this.pets.findById(petNaranja.getId());

		assertNotNull(petNaranjaFindById.getName());
		assertEquals(petNaranjaFindById.getName(), petNaranja.getName());

		assertEquals(petNaranjaFindById.getBirthDate(), petNaranja.getBirthDate());

		assertEquals(petNaranjaFindById.getComments(), petNaranja.getComments());

		assertEquals(petNaranjaFindById.getOwner(), petNaranja.getOwner());

		assertEquals(petNaranjaFindById.getType(), petNaranja.getType());

		assertTrue(petNaranjaFindById.getWeight() == petNaranja.getWeight());

		assertEquals(petNaranjaFindById.getVisits().size(), petNaranja.getVisits().size());

		if (petNaranjaFindById.getVisits().size() > 0) {
			assertArrayEquals(petNaranjaFindById.getVisits().toArray(), petNaranja.getVisits().toArray());
		}

		Visit visitNaranjaFindById = this.visits.findById(visitNaranja.getId());

		assertEquals(visitNaranjaFindById.getDate(), visitNaranja.getDate());

		assertEquals(visitNaranjaFindById.getDescription(), visitNaranja.getDescription());

		assertEquals(visitNaranjaFindById.getPetId(), visitNaranja.getPetId());

		assertEquals(visitNaranjaFindById.getVet(), visitNaranja.getVet());

	}

	@Test
	public void testEditVisitbyId() {

		Visit visitNaranjaFindById = this.visits.findById(visitNaranja.getId());

		LocalDate localdatActual = visitNaranjaFindById.getDate();
		String desActual = visitNaranjaFindById.getDescription();
		Vet vetpruebaActual = visitNaranjaFindById.getVet();

		LocalDate localdatemod = LocalDate.MAX;
		String des = "Revisión de las alas más completa";
		Vet vetprueba = new Vet();
		vetprueba.setFirstName("Miguel");
		vetprueba.setLastName("González");
		vetprueba.setHomeVisits(true);
		Collection<Specialty> vetSpec = this.vets.findSpecialties();

		if (!vetSpec.isEmpty()) {
			for (Specialty spec : vetSpec) {
				vetprueba.addSpecialty(spec);
			}
		}

		visitNaranjaFindById.setDate(localdatemod);
		visitNaranjaFindById.setDescription(des);
		visitNaranjaFindById.setVet(vetprueba);

		assertNotEquals(localdatActual, visitNaranjaFindById.getDate());
		assertNotEquals(desActual, visitNaranjaFindById.getDescription());
		assertNotEquals(vetpruebaActual, visitNaranjaFindById.getVet());

		visitNaranja = visitNaranjaFindById;

		this.visits.save(visitNaranja);

		Visit visitNaranjaFindById2 = this.visits.findById(visitNaranja.getId());

		assertEquals(visitNaranjaFindById2.getDate(), visitNaranja.getDate());

		assertEquals(visitNaranjaFindById2.getDescription(), visitNaranja.getDescription());

		assertEquals(visitNaranjaFindById2.getPetId(), visitNaranja.getPetId());

		assertEquals(visitNaranjaFindById2.getVet(), visitNaranja.getVet());
	}

	@After
	public void finish() {
		this.vetNaranja = null;
		this.ownerNaranja = null;
		this.petNaranja = null;
		this.visitNaranja = null;

		this.petMarron = null;
		this.ownerMarron = null;

		this.ownerBlanco = null;
		this.petBlanco = null;

		this.petAzul = null;
		this.ownerAzul = null;
	}
}
