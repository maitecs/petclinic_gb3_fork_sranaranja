package org.springframework.samples.petclinic.selenium;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class CasodeUso3 {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
		driver = new ChromeDriver();
		baseUrl = "https://www.katalon.com/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testCasodeUso3() throws Exception {
		driver.get("http://localhost:8380/");
		driver.findElement(By.xpath(
				"(.//*[normalize-space(text()) and normalize-space(.)='Toggle navigation'])[1]/following::span[7]"))
				.click();
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/following::span[2]"))
				.click();
		driver.findElement(By.linkText("Add Owner")).click();
		driver.findElement(By.id("firstName")).click();
		driver.findElement(By.id("firstName")).clear();
		driver.findElement(By.id("firstName")).sendKeys("Isabel");
		driver.findElement(By.id("lastName")).click();
		driver.findElement(By.id("lastName")).clear();
		driver.findElement(By.id("lastName")).sendKeys("Guerrero");
		driver.findElement(By.id("address")).click();
		driver.findElement(By.id("address")).clear();
		driver.findElement(By.id("address")).sendKeys("C/Soledad, 56");
		driver.findElement(By.id("city")).click();
		driver.findElement(By.id("city")).clear();
		driver.findElement(By.id("city")).sendKeys("Almendralejo");
		driver.findElement(By.id("telephone")).click();
		driver.findElement(By.id("telephone")).clear();
		driver.findElement(By.id("telephone")).sendKeys("888888888");
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Telephone'])[1]/following::button[1]"))
				.click();
		driver.findElement(By.linkText("Add New Pet")).click();
		assertEquals("Owner",
				driver.findElement(By
						.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Error'])[1]/following::label[1]"))
						.getText());
		assertEquals("Isabel Guerrero",
				driver.findElement(By
						.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Owner'])[1]/following::span[1]"))
						.getText());
		driver.findElement(By.id("name")).click();
		driver.findElement(By.id("name")).clear();
		driver.findElement(By.id("name")).sendKeys("Tim");
		driver.findElement(By.id("birthDate")).click();
		driver.findElement(By.id("birthDate")).clear();
		driver.findElement(By.id("birthDate")).sendKeys("2010-06-07");
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Birth Date'])[1]/following::label[1]"))
				.click();
		driver.findElement(By.id("type")).click();
		new Select(driver.findElement(By.id("type"))).selectByVisibleText("hamster");
		driver.findElement(By.id("weight")).click();
		driver.findElement(By.id("weight")).clear();
		driver.findElement(By.id("weight")).sendKeys("3.0");
		driver.findElement(By.id("comments")).click();
		driver.findElement(By.id("comments")).clear();
		driver.findElement(By.id("comments")).sendKeys("come demasiado");
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Error'])[1]/following::form[1]"))
				.click();
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[1]/following::button[1]"))
				.click();
		driver.findElement(By.linkText("Add New Pet")).click();
		driver.findElement(By.id("name")).click();
		driver.findElement(By.id("name")).clear();
		driver.findElement(By.id("name")).sendKeys("Lili");
		driver.findElement(By.id("birthDate")).click();
		driver.findElement(By.id("birthDate")).clear();
		driver.findElement(By.id("birthDate")).sendKeys("2007-05-04");
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Birth Date'])[1]/following::label[1]"))
				.click();
		driver.findElement(By.id("type")).click();
		new Select(driver.findElement(By.id("type"))).selectByVisibleText("snake");
		driver.findElement(By.id("weight")).click();
		driver.findElement(By.id("weight")).clear();
		driver.findElement(By.id("weight")).sendKeys("2.0");
		driver.findElement(By.id("comments")).click();
		driver.findElement(By.id("comments")).clear();
		driver.findElement(By.id("comments")).sendKeys("tiene mal color");
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Error'])[1]/following::div[3]"))
				.click();
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[1]/following::button[1]"))
				.click();
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/following::span[2]"))
				.click();
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Last name'])[1]/following::button[1]"))
				.click();
		assertEquals("Owners",
				driver.findElement(
						By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Error'])[1]/following::h2[1]"))
						.getText());
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	private boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}
}
