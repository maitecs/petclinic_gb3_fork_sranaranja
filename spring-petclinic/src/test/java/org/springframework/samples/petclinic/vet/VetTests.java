/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.vet;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.PetType;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.util.SerializationUtils;

import static org.assertj.core.api.Assertions.assertThat;
import java.time.LocalDate;

/**
 * @author Dave Syer
 *
 */
public class VetTests {

	//Declaración de variables necesarias para la prueba de asignar una visita a un veterinario
	public static Vet vetNaranja;
	public static Pet petNaranja;
	public static Owner ownerNaranja;
	public static Visit visitNaranja;
	public static PetType pettypeNaranja;
	public static Specialty specialtyNaranja;
	public static Vet vetMarron;
	public static Specialty specialtyMarron;
	public static Vet vetNaranja2;
	public static Specialty specialtyNaranja2;
	
	@BeforeClass
	//This method is called before executing this suite
	public static void initClass() {
		
		//Creación del veterinario:
		vetNaranja = new Vet();
		vetNaranja.setId(3004);
		vetNaranja.setFirstName("Francisco");
		vetNaranja.setLastName("López");
		vetNaranja.setHomeVisits(true);
		specialtyNaranja = new Specialty();
		specialtyNaranja.setId(3005);
		specialtyNaranja.setName("otorrino");
		vetNaranja.addSpecialty(specialtyNaranja);
		
		//Creación del propietario:
		ownerNaranja = new Owner();
		ownerNaranja.setId(3000);
		ownerNaranja.setFirstName("Pedro");
		ownerNaranja.setLastName("Sánchez");
		ownerNaranja.setAddress("Av. Portugal, 7");
		ownerNaranja.setCity("Cáceres");
		ownerNaranja.setTelephone("333333333");
		
		//Creación de la mascota:
		petNaranja = new Pet();
		petNaranja.setId(3001);
		petNaranja.setName("Pepi");
		LocalDate localdate = LocalDate.now();
		petNaranja.setBirthDate(localdate);
		petNaranja.setComments("Es un poco agresiva");
		pettypeNaranja = new PetType(); //tipo de la mascota
		pettypeNaranja.setId(3002);
		pettypeNaranja.setName("perro");
		petNaranja.setType(pettypeNaranja);
		petNaranja.setWeight(15);
		
		//Asigno mascota al propietario:
		ownerNaranja.addPet(petNaranja);
		
		//Creación de la visita:
		visitNaranja = new Visit();
		visitNaranja.setId(3003);
		visitNaranja.setDate(localdate);
		visitNaranja.setDescription("Revisión de las orejas");
		visitNaranja.setVet(vetNaranja);
		

		// Creación del veterinario:
		vetNaranja2 = new Vet();
		vetNaranja2.setId(3006);
		vetNaranja2.setFirstName("Jesús");
		vetNaranja2.setLastName("Díaz");
		vetNaranja2.setHomeVisits(true);
		specialtyNaranja2 = new Specialty();
		specialtyNaranja2.setId(3007);
		specialtyNaranja2.setName("cirujano");
		vetNaranja2.addSpecialty(specialtyNaranja2);

		vetMarron = new Vet();
		vetMarron.setFirstName("Manuel");
		vetMarron.setLastName("Diaz");
		vetMarron.setHomeVisits(false);
		specialtyMarron = new Specialty();
		specialtyMarron.setName("Dentista");
		vetMarron.addSpecialty(specialtyMarron);
	}
	
    @Test
    public void testSerialization() {
        Vet vet = new Vet();
        vet.setFirstName("Zaphod");
        vet.setLastName("Beeblebrox");
        vet.setId(123);
        Vet other = (Vet) SerializationUtils
                .deserialize(SerializationUtils.serialize(vet));
        assertThat(other.getFirstName()).isEqualTo(vet.getFirstName());
        assertThat(other.getLastName()).isEqualTo(vet.getLastName());
        assertThat(other.getId()).isEqualTo(vet.getId());
    }
    
    @Test
	public void testVet() {
		assertNotNull(visitNaranja.getVet());	
		assertEquals(vetNaranja.getFirstName(), visitNaranja.getVet().getFirstName());
	}
    
    //Caso de uso 12: insertar un veterinario
    @Test
	public void testInsertarVet() {
		assertNotNull(vetNaranja.getFirstName());	
		assertEquals(vetNaranja.getFirstName(), "Francisco");
		
		assertNotNull(vetNaranja.getLastName());	
		assertEquals(vetNaranja.getLastName(), "López");
		
		assertNotNull(vetNaranja.getSpecialties());	
		assertEquals(vetNaranja.getSpecialties().size(), 1);
	}
    
    @Test
	public void testVet2() {
		assertNotNull(vetNaranja2.getHomeVisits());
		assertEquals(vetNaranja2.getHomeVisits(), true);
	}

	@Test
	public void testVetNR2() {
		assertNotNull(vetMarron);
		assertEquals("Manuel", vetMarron.getFirstName());
		assertEquals("Diaz", vetMarron.getLastName());
	}

	@Test
	public void testSpecialtyNR2() {
		assertNotNull(specialtyMarron);
		assertEquals("Dentista", specialtyMarron.getName());
	}

	
    @AfterClass
    //This method is executed after all the tests included in this suite are completed.
    public static void finishClass() {
    	vetNaranja = null;
    	petNaranja = null;
    	ownerNaranja = null;
    	visitNaranja = null;
    	pettypeNaranja = null;
    	specialtyNaranja = null;
		vetNaranja2 = null;
		specialtyNaranja2 = null;

    }
	
	

	

}
