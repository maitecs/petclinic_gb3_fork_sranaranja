package org.springframework.samples.petclinic.selenium;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

//Corregido un error en el enlace a Jira, que apuntaba a la tarea principal en vez de a la subtarea
public class GB3SeleniumNR4 {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
	System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
    driver = new ChromeDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testGB3SeleniumNR4() throws Exception {
    driver.get("http://localhost:8380/");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/following::span[2]")).click();
    assertEquals("Add Owner", driver.findElement(By.linkText("Add Owner")).getText());
    driver.findElement(By.linkText("Add Owner")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Owner'])[1]/following::div[1]")).click();
    driver.findElement(By.id("firstName")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Error'])[1]/following::div[2]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Error'])[1]/following::div[1]")).click();
    assertEquals("Owner", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Error'])[1]/following::h2[1]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Owner'])[1]/following::label[1]")).click();
    assertEquals("First Name", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Owner'])[1]/following::label[1]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='First Name'])[1]/following::label[1]")).click();
    assertEquals("Last Name", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='First Name'])[1]/following::label[1]")).getText());
    assertEquals("Address", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Last Name'])[1]/following::label[1]")).getText());
    assertEquals("City", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Address'])[1]/following::label[1]")).getText());
    assertEquals("Telephone", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='City'])[1]/following::label[1]")).getText());
    driver.findElement(By.id("firstName")).click();
    driver.findElement(By.id("firstName")).clear();
    driver.findElement(By.id("firstName")).sendKeys("Juan Carlos");
    driver.findElement(By.id("lastName")).clear();
    driver.findElement(By.id("lastName")).sendKeys("Arias Holguin");
    driver.findElement(By.id("address")).clear();
    driver.findElement(By.id("address")).sendKeys("Concordia");
    driver.findElement(By.id("city")).clear();
    driver.findElement(By.id("city")).sendKeys("Caceres");
    driver.findElement(By.id("telephone")).clear();
    driver.findElement(By.id("telephone")).sendKeys("123456789");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Telephone'])[1]/following::button[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Owner Information'])[1]/following::th[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Juan Carlos Arias Holguin'])[1]/following::th[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Juan Carlos Arias Holguin'])[1]/following::th[1]")).click();
    assertEquals("Address", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Juan Carlos Arias Holguin'])[1]/following::th[1]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Concordia'])[1]/following::th[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Concordia'])[1]/following::th[1]")).click();
    assertEquals("City", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Concordia'])[1]/following::th[1]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Caceres'])[1]/following::th[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Caceres'])[1]/following::th[1]")).click();
    assertEquals("Telephone", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Caceres'])[1]/following::th[1]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::td[1]")).click();
    assertEquals("Juan Carlos Arias Holguin", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::b[1]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Address'])[1]/following::td[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Address'])[1]/following::td[1]")).click();
    assertEquals("Concordia", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Address'])[1]/following::td[1]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='City'])[1]/following::td[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='City'])[1]/following::td[1]")).click();
    assertEquals("Caceres", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='City'])[1]/following::td[1]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Telephone'])[1]/following::td[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Telephone'])[1]/following::td[1]")).click();
    assertEquals("123456789", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Telephone'])[1]/following::td[1]")).getText());
    assertEquals("Edit Owner", driver.findElement(By.linkText("Edit Owner")).getText());
    assertEquals("Delete Owner", driver.findElement(By.linkText("Delete Owner")).getText());
    assertEquals("Add New Pet", driver.findElement(By.linkText("Add New Pet")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Add New Pet'])[1]/following::h2[1]")).click();
    assertEquals("Pets and Visits", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Add New Pet'])[1]/following::h2[1]")).getText());
    driver.findElement(By.linkText("Delete Owner")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Error'])[1]/following::div[2]")).click();
    assertEquals("Find Owners", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Error'])[1]/following::h2[1]")).getText());
    assertEquals("Last name", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Find Owners'])[1]/following::label[1]")).getText());
    assertEquals("Find Owner", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Last name'])[1]/following::button[1]")).getText());
    assertEquals("Add Owner", driver.findElement(By.linkText("Add Owner")).getText());
    driver.findElement(By.id("lastName")).click();
    driver.findElement(By.id("lastName")).clear();
    driver.findElement(By.id("lastName")).sendKeys("Arias Holguin");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Last name'])[1]/following::button[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Last name'])[1]/following::p[1]")).click();
    assertEquals("has not been found", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Last name'])[1]/following::p[1]")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
