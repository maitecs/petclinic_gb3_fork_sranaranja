/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.owner;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.vet.Specialty;
import org.springframework.util.SerializationUtils;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Dave Syer
 *
 */
public class OwnerTests {

	public static Owner ownerMarron;
	@Before
	public void init() {
		ownerMarron = new Owner();
		ownerMarron.setCity("City");
		ownerMarron.setFirstName("Pepe");
		ownerMarron.setLastName("Alfaro");
		ownerMarron.setTelephone("698765432");
		ownerMarron.setAddress("Address");
	}

	@Test
	public void testOwnerNR4() {
		assertNotNull(ownerMarron);
	}

	@Test
	public void testPetsNR4() {
		assertNotNull(ownerMarron);
		assertEquals(0, ownerMarron.getPets().size());
	}
}
