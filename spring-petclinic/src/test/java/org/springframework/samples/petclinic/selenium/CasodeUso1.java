package org.springframework.samples.petclinic.selenium;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class CasodeUso1 {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
	System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
	driver = new ChromeDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testCasodeUso1() throws Exception {
    driver.get("http://localhost:8380/");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Toggle navigation'])[1]/following::span[7]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/following::span[2]")).click();
    driver.findElement(By.linkText("Add Owner")).click();
    driver.findElement(By.id("firstName")).click();
    driver.findElement(By.id("firstName")).clear();
    driver.findElement(By.id("firstName")).sendKeys("Luis");
    driver.findElement(By.id("lastName")).click();
    driver.findElement(By.id("lastName")).clear();
    driver.findElement(By.id("lastName")).sendKeys("Rodríguez");
    driver.findElement(By.id("address")).click();
    driver.findElement(By.id("address")).clear();
    driver.findElement(By.id("address")).sendKeys("C/Las Rocas, 20");
    driver.findElement(By.id("city")).click();
    driver.findElement(By.id("city")).clear();
    driver.findElement(By.id("city")).sendKeys("Cáceres");
    driver.findElement(By.id("telephone")).click();
    driver.findElement(By.id("telephone")).clear();
    driver.findElement(By.id("telephone")).sendKeys("999999999");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Telephone'])[1]/following::button[1]")).click();
    assertEquals("Add New Pet", driver.findElement(By.linkText("Add New Pet")).getText());
    driver.findElement(By.linkText("Add New Pet")).click();
    assertEquals("Owner", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Error'])[1]/following::label[1]")).getText());
    assertEquals("Luis Rodríguez", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Owner'])[1]/following::span[1]")).getText());
    driver.findElement(By.id("name")).click();
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Lima");
    driver.findElement(By.id("birthDate")).click();
    driver.findElement(By.id("birthDate")).clear();
    driver.findElement(By.id("birthDate")).sendKeys("2016-03-20");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Birth Date'])[1]/following::label[1]")).click();
    driver.findElement(By.id("type")).click();
    new Select(driver.findElement(By.id("type"))).selectByVisibleText("lizard");
    driver.findElement(By.id("type")).click();
    driver.findElement(By.id("weight")).click();
    driver.findElement(By.id("weight")).clear();
    driver.findElement(By.id("weight")).sendKeys("7.0");
    driver.findElement(By.id("comments")).click();
    driver.findElement(By.id("comments")).clear();
    driver.findElement(By.id("comments")).sendKeys("escupe");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[1]/following::button[1]")).click();
    driver.findElement(By.linkText("Edit Pet")).click();
    assertEquals("Weight", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Type'])[1]/following::label[1]")).getText());
    assertEquals("7.0", driver.findElement(By.id("weight")).getAttribute("value"));
    assertEquals("Comments", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Weight'])[1]/following::label[1]")).getText());
    assertEquals("escupe", driver.findElement(By.id("comments")).getAttribute("value"));
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[1]/following::button[1]")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
