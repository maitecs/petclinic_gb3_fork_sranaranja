package org.springframework.samples.petclinic.owner;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.owner.OwnerRepository;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class OwnerRepositoryTest {

	@Autowired
	private OwnerRepository owners;

	private Owner ownerMarron;

	@Before
	public void init() {

		if (this.ownerMarron == null) {
			ownerMarron = new Owner();
			ownerMarron.setCity("City");
			ownerMarron.setFirstName("Pepe");
			ownerMarron.setLastName("Alfaro");
			ownerMarron.setTelephone("698765432");
			ownerMarron.setAddress("Address");

			this.owners.save(ownerMarron);
		}
	}

	@Test
	public void testOwnerNR4() {
		Owner ownerFindById = this.owners.findById(ownerMarron.getId());

		assertNotNull(ownerFindById);
		assertEquals(0, ownerFindById.getPets().size());

		// Si no hay mascotas
		if (ownerFindById.getPets().size() == 0) {
			this.owners.deleteById(ownerFindById.getId());
			assertNull(this.owners.findById(ownerMarron.getId()));
		}
	}

	@After
	public void finish() {
		this.ownerMarron = null;
	}

}
